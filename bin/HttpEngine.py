import requests
import concurrent.futures
import time


class HttpAsyncEngine:

    def load_url(self, url):
        try:
            r = requests.get(url,timeout=10)
            if r.status_code == 200:
                return (r.content,[])
            # if error
            else:
                return ([], url)
        except Exception as e:
            print(e)

    def mine_url_list(self, data_list):
        retData = {}
        retData[0]=[]
        retData[1] = []
        raw_data=self.async_pool(self.load_url,data_list,30)
        if len(raw_data)!=0:
            for data in raw_data[0]:
                if len(data) !=0:
                    retData[0].append(data)
                else:
                    retData[1].append(data)
        return retData

    def async_pool(self, method, data_list,sleep=None):
        retData = []
        with concurrent.futures.ThreadPoolExecutor(max_workers=20) as executor:
            futures = []
            for data in data_list:
                futures.append(executor.submit(method, data))
            for future in concurrent.futures.as_completed(futures):
                try:
                    data = future.result()
                    if data is not None:
                        retData.append(data)
                except Exception as e:
                    print(e)
                    if sleep is None:
                        time.sleep(1)
                    else:
                        time.sleep(sleep)
        return retData


if __name__ == "__main__":
    print("test")
