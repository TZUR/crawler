from bs4 import BeautifulSoup
from bin.HttpEngine import HttpAsyncEngine
import re, os, csv
from datetime import datetime


class Crawler:

    def __init__(self, start_url, depth):
        self.depth = depth
        self.engine = HttpAsyncEngine()
        self.links_depth = {}
        self.start_url = start_url
        self.report = {}
        self.report['target_site'] = start_url
        self.report['links'] = {}
        self.report['outerlinks'] = {}
        self.report['broken_links'] = {}
        self.report['images'] = {}
        self.report['images']['duplicates'] = {}

    def run(self):
        target_urls = ["http://" + self.start_url]
        existing_image = {}
        for i in range(0, self.depth):
            print("Layer number:" + str(i))
            links = self.run_layer(target_urls)
            layer_images = links[2]
            for img in layer_images:
                image_name = os.path.basename(img[0])
                if image_name is not existing_image:
                    existing_image[image_name] = img
                else:
                    if existing_image[image_name] is not img:
                        print("Found duplicate image: " + existing_image[image_name] + " " + img)
                        self.report['images']['duplicates'][existing_image[image_name]] = img
            # if link exists
            for link in links[0][0][0]:
                if link not in self.report['links']:
                    self.report['links'][link] = i
                    print("New Link " + link)
            for link in links[0][0][1]:
                if link not in self.report['links']:
                    self.report['outerlinks'][link] = i
                    print("New Link " + link)
            # broken links
            for link in links[1]:
                if link not in self.report['links']:
                    self.report['broken_links'][link] = i
                    print("Broken Link " + link)
            target_urls = links[0][0][0]
            print("Layer number:" + str(i) + " finished")
        self.generate_report()
        return self.report

    def generate_report(self):
        date = datetime.now().utcnow().strftime("%s")
        report_name = self.start_url + "_" + str(date)
        with open('../output/' + report_name + "_links.csv", 'a+') as f:
            f.write("%s,%s\n" % ("link", "depth"))
            for key in self.report['links'].keys():
                f.write("%s,%s\n" % (key, self.report['links'][key]))
        with open('../output/' + report_name + "_outerlinks.csv", 'a+') as f:
            f.write("%s,%s\n" % ("link", "depth"))
            for key in self.report['outerlinks'].keys():
                f.write("%s,%s\n" % (key, self.report['outerlinks'][key]))
        with open('../output/' + report_name + "_broken_links.csv", 'a+') as f:
            f.write("%s,%s\n" % ("link", "depth"))
            for key in self.report['broken_links'].keys():
                f.write("%s,%s\n" % (key, self.report['outerlinks'][key]))

    def fetch_images(self, html_page):
        soup = BeautifulSoup(html_page, 'html.parser')
        img_tags = soup.find_all('img')
        urls = [img['src'] for img in img_tags]
        return urls

    def fetch_links(self, html_page):
        soup = BeautifulSoup(html_page, 'html.parser')
        links = []
        outer_links = []
        regex = "^(https|http)://"
        for link in soup.find_all('a', {'href': re.compile(regex)}):
            v_link = link.get('href')
            if self.start_url in v_link:
                links.append(v_link)
            else:
                outer_links.append(v_link)
        return links, outer_links

    def run_layer(self, urls_list):
        layer_links = []
        layer_broken_links = []
        valid_data = []
        layer_images = []
        eng = HttpAsyncEngine()
        raw_data = eng.mine_url_list(urls_list)
        for data in raw_data[0]:
            if len(data) != 0:
                valid_data.append(data)
            else:
                layer_broken_links.append(data[1])
        layer_images.extend(eng.async_pool(self.fetch_images, valid_data))
        layer_links.extend(eng.async_pool(self.fetch_links, valid_data))
        return layer_links, layer_broken_links, layer_images


if __name__ == "__main__":
    crawler = Crawler('walla.co.il', 2)
    crawler.run()
    print("done")
